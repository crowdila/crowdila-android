package com.android.crowdila;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;

import com.google.android.flexbox.FlexDirection;
import com.google.android.flexbox.FlexWrap;
import com.google.android.flexbox.FlexboxLayoutManager;
import com.google.android.flexbox.JustifyContent;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class TagsActivity extends AppCompatActivity {
    TagAdapter adapter;
    RecyclerView recyclerView;
    List<Tag>tagList;
    Button save;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tags);
        recyclerView = findViewById(R.id.recyclerViewTags);
        save = findViewById(R.id.saveTagsBT);
        tagList = new ArrayList<>();
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<Tag>selected = new ArrayList<>();
                for(int i=0;i<adapter.getTagsList().size();i++)
                    if(adapter.getTagsList().get(i).isSelected())
                        selected.add(adapter.getTagsList().get(i));
                saveTags(selected);
            }
        });
        load();
    }

    protected void load()
    {
        GraphApi.fetchTags(this, new API.RequestHandler() {
            @Override
            public void Handle(JSONObject obj) {
                try {
                    obj = obj.getJSONObject("data");
                    JSONArray arr = obj.getJSONArray("tags");
                    for (int i = 0; i < arr.length(); i++) {
                        Tag t = new Tag();
                        t.setName(arr.getJSONObject(i).getString("name"));
                        tagList.add(t);
                    }
                    adapter = new TagAdapter(TagsActivity.this,tagList);
                    recyclerView.setAdapter(adapter);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        loadList();
    }

    protected void loadList()
    {
        FlexboxLayoutManager layoutManager = new FlexboxLayoutManager(this);
        layoutManager.setFlexDirection(FlexDirection.ROW);
        layoutManager.setJustifyContent(JustifyContent.SPACE_EVENLY);
        layoutManager.setFlexWrap(FlexWrap.WRAP);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new TagAdapter(this,tagList);
        recyclerView.setAdapter(adapter);
    }

    protected void saveTags(List<Tag> tags)
    {
        GraphApi.updateTags(this, new API.RequestHandler() {
            @Override
            public void Handle(JSONObject obj) {
                String origin = getIntent().getStringExtra("FROM");
                if (origin!=null && origin.equals("SignUp")) {
                    Intent i = new Intent(getApplicationContext(), answerQuestion.class);
                    i.putExtra("postID", -1);
                    startActivity(i);
                    finish();
                }
                else{
                    finish();
                }
            }
        }, tags);
    }

}
