package com.android.crowdila;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.flexbox.FlexDirection;
import com.google.android.flexbox.FlexWrap;
import com.google.android.flexbox.FlexboxLayout;
import com.google.android.flexbox.FlexboxLayoutManager;
import com.google.android.flexbox.JustifyContent;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class Profile extends AppCompatActivity {

    List<Imagee> urlList;
    logoAdapter logoAdapter ;
    RecyclerView recyclerView;
    ImageView img;
    TextView txv;
    String username;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        img = findViewById(R.id.CIV);
        txv = findViewById(R.id.username);
        SharedPreferences pref = getSharedPreferences("user", MODE_PRIVATE);
        username = pref.getString("username", null) ;
        txv.setText(username);
        recyclerView =findViewById(R.id.recyclerView);
        Picasso.get().load("https://s3.amazonaws.com/crowdila-avatars/14.png").into(img);
        urlList = new ArrayList<>();
        urlList.add(new Imagee(
                1,
                new String("https://s3.amazonaws.com/crowdila-avatars/1.png")
        ));
        urlList.add(new Imagee(
                1,
                new String("https://s3.amazonaws.com/crowdila-avatars/2.png")
        ));
        urlList.add(new Imagee(
                1,
                new String("https://s3.amazonaws.com/crowdila-avatars/3.png")
        ));
        urlList.add(new Imagee(
                1,
                new String("https://s3.amazonaws.com/crowdila-avatars/4.png")
        ));

        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            recyclerView = findViewById(R.id.recyclerView);
            recyclerView.setHasFixedSize(true);
            FlexboxLayoutManager layoutManager = new FlexboxLayoutManager(Profile.this);
            layoutManager.setFlexDirection(FlexDirection.ROW);
            layoutManager.setJustifyContent(JustifyContent.SPACE_EVENLY);
            layoutManager.setFlexWrap(FlexWrap.WRAP);
            recyclerView.setLayoutManager(layoutManager);
            recyclerView.setVisibility(View.VISIBLE);
            logoAdapter = new logoAdapter(Profile.this, urlList);
            recyclerView.setAdapter(logoAdapter);
        }});
        Button button = findViewById(R.id.change);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(),TagsActivity.class));
                }
        });
    }
}

