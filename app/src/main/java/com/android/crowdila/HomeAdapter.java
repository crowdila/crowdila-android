package com.android.crowdila;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

public class HomeAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    @NonNull
    List<Post>posts;
    Context context;
    RecyclerView recyclerView;
    private AdapterView.OnItemClickListener onItemClickListener;
    int lastPosition = -1;
    boolean isLoading;
    private OnLoadMoreListener onLoadMoreListener;
    int lodpos=-1;
    Post loading;

    public HomeAdapter(Context context, final List<Post> posts, RecyclerView recyclerView, AdapterView.OnItemClickListener onItemClickListener)
    {
        this.context = context;
        this.posts = posts;
        this.recyclerView = recyclerView;
        this.onItemClickListener = onItemClickListener;
        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        loading = new Post();
        loading.setLoading(true);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                if (!isLoading && lastVisibleItem==getItemCount()-1) {
                    if (onLoadMoreListener != null) {
                        onLoadMoreListener.onLoadMore();
                    }
                    isLoading = true;
                    lodpos = posts.size();
                    posts.add(loading);
                    notifyDataSetChanged();
                }
            }
        });
    }


    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.onLoadMoreListener = mOnLoadMoreListener;
    }

    public void setLoaded() {
        isLoading = false;
        if(lodpos!=-1)
        {
            posts.remove(lodpos);
            lodpos = -1;
            notifyDataSetChanged();
        }
    }

    List<Post> getData()
    {
        return posts;
    }

    @Override
    public int getItemViewType(int position) {
        return posts.get(position).isLoading() ? 0 : 1;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater;
        inflater = LayoutInflater.from(parent.getContext());
        View view;
        if(viewType==1)
            view = inflater.inflate(R.layout.home_list_item, parent, false);
        else
            view = inflater.inflate(R.layout.home_list_item_load,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder vholder, int position) {
        final ViewHolder holder = (ViewHolder) vholder;
        if(vholder.getItemViewType()==1) {
            holder.layout.setVisibility(View.GONE);
            holder.Qtv.setVisibility(View.GONE);
            if (holder.layout.getChildCount() > 0)
                holder.layout.removeAllViews();
            setAnimation(holder.itemView, position);
            Post p = posts.get(position);
            holder.textView.setText(p.getTitle());
            if (p.getQuestionCount() > 1) {
                for (int i = 0; i < p.getQuestionCount() && i < 5; i++) {
                    View v = LayoutInflater.from(context).inflate(R.layout.dot, null);
                    holder.layout.addView(v);
                }
                if (p.getQuestionCount() > 5) {
                    View v = LayoutInflater.from(context).inflate(R.layout.dot, null);
                    ImageView iv = v.findViewById(R.id.DotIV);
                    iv.setImageResource(R.drawable.add_c);
                    holder.layout.addView(v);
                }
                holder.layout.setVisibility(View.VISIBLE);
                holder.Qtv.setVisibility(View.VISIBLE);
            }
        }else
        {
            Animation a = AnimationUtils.loadAnimation(context,R.anim.rotate);
            a.setRepeatCount(Animation.INFINITE);
            holder.loadIV.startAnimation(a);
        }
    }

    @Override
    public int getItemCount() {
        return posts.size();
    }

    public void addItem(Post post)
    {
        if(lodpos!=-1)
        {
            posts.remove(lodpos);
            lodpos = -1;
            notifyDataSetChanged();
        }
        posts.add(post);
        notifyDataSetChanged();
    }

    private void setAnimation(View viewToAnimate, int position)
    {
        if (position > lastPosition)
        {
            Animation animation = AnimationUtils.loadAnimation(context, R.anim.push_item);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

    protected class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        protected TextView textView,Qtv;
        protected LinearLayout layout;
        protected ImageView loadIV;

        public ViewHolder(final View itemView) {
            super(itemView);
            if(itemView.getId()==R.id.homeListItem) {
                textView = itemView.findViewById(R.id.HLICTV);
                layout = itemView.findViewById(R.id.HLILL);
                Qtv = itemView.findViewById(R.id.HLIQ);
            } else
                loadIV = itemView.findViewById(R.id.HLIload);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onItemClickListener.onItemClick(null, view, getAdapterPosition(), view.getId());
                }
            });
        }

        @Override
        public void onClick(View v) {
            onItemClickListener.onItemClick(null,v,getAdapterPosition(),v.getId());
        }
    }

    public interface OnLoadMoreListener {
        void onLoadMore();
    }
}
