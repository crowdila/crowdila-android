package com.android.crowdila;

import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class answerQuestion extends AppCompatActivity {
    public static class QuestionAnswer {
        public List<Integer> optionIds;
        public String text;
        public int questionId;

        public QuestionAnswer(int qid, List<Integer> l) {
            optionIds = l;
            questionId = qid;
        }

        public QuestionAnswer(int qid, String t) {
            text = t;
            questionId = qid;
        }

        public JSONObject toJson() {
            JSONObject ret = new JSONObject();
            try {
                ret.put("questionId", questionId);
                if(text != null) {
                    ret.put("text", text);
                } else {
                    JSONArray arr = new JSONArray();
                    for (int i = 0; i < optionIds.size(); i++) {
                        arr.put(optionIds.get(i));
                    }
                    ret.put("optionIds", arr);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return ret;
        }
    }

    List<Question> questionList;
    RecyclerView  recyclerView ;
    answerAdapter adapter;
    MultiChoiceAdapter multiChoiceAdapter;
    TextView textView;
    EditText  textAnswer, addText;
    int position = 0,postId=-1;
    ConstraintLayout addAns;
    Button button, add;
    boolean test = false ;
    List<QuestionAnswer> qas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_answer_question);
        questionList = new ArrayList<>();
        textView = findViewById(R.id.textView);
        textAnswer = findViewById(R.id.textAnswer);
        recyclerView = findViewById(R.id.recyclerView);
        addAns =  findViewById(R.id.addAns);
        button = findViewById(R.id.nextButton);
        add = findViewById(R.id.add);
        addText = findViewById(R.id.addtext);
        postId = getIntent().getIntExtra("postID",-1);

        qas = new ArrayList<>();

        addAns.setVisibility(View.INVISIBLE);
        textAnswer.setVisibility(View.INVISIBLE);

        postId = getIntent().getIntExtra("postID", -1);

        if(postId == -1) {
            // Dummy question
            Question q = new Question();
            q.setId(-1);
            q.setQuestionType(QuestionType.MCQ);
            q.setText("What is the airspeed velocity of an unladen swallow?");
            Option op1 = new Option();
            op1.setId(-1);
            op1.setText("11.3 m/s");
            Option op2 = new Option();
            op2.setId(-1);
            op2.setText("I don't know");
            Option op3 = new Option();
            op3.setId(-1);
            op3.setText("African or European?");

            q.addOption(op1);
            q.addOption(op2);
            q.addOption(op3);
            questionList.add(q);

            start();
        } else {
            loadPost(postId);
        }

        add.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v){
                Option op = new Option();
                op.setText(addText.getText().toString());
                op.setVots(0);
                questionList.get(position).addOption(op);

                if (questionList.get(position).isMulti_choice()) {

                    multiChoiceAdapter.notifyDataSetChanged();
                } else {
                    adapter.notifyDataSetChanged();
                }
            }
        });
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (test || position+1 == questionList.size()) {
                    sendAnswers();
                    startActivity(new Intent(getApplicationContext(),MainActivity.class));
                    //finish();
                    return;
                }
                else {
                    textAnswer.setText("");
                }
                textAnswer.setVisibility(View.INVISIBLE);
                addAns.setVisibility(View.INVISIBLE);
                addText.setText("");

                if (questionList.get(position).getQuestionType() == QuestionType.MCQ) {
                    if (questionList.get(position).isMulti_choice()) {
                        qas.add(new QuestionAnswer(questionList.get(position).getId(), multiChoiceAdapter.getOptionId()));
                        multiChoiceAdapter.rest();
                    } else {
                        qas.add(new QuestionAnswer(questionList.get(position).getId(), adapter.getOptionId()));
                        adapter.rest();
                    }
                } else {
                    textAnswer.setVisibility(View.VISIBLE);
                }

                position++;

                if (position + 1 == questionList.size()) {
                    button.setText("DONE");
                    test = true;
                }
                if (questionList.get(position).getQuestionType() == QuestionType.TEXT) {
                    textAnswer.setVisibility(View.VISIBLE);
                    textView.setText(questionList.get(position).getText());
                    recyclerView.setVisibility(View.INVISIBLE);
                } else {
                    if (questionList.get(position).isOpen_answer()) {
                        addAns.setVisibility(View.VISIBLE);
                    }
                    textAnswer.setVisibility(View.INVISIBLE);
                    recyclerView.setVisibility(View.VISIBLE);
                    displayQuestion(position, questionList.get(position).isMulti_choice());
                }
            }
        });

    }

    public void createAnswer(int question_id, List<Integer> option_id, String text){

    }

    private void loadPost(int postId) {
        GraphApi.fetchPost(this, new API.RequestHandler() {
            @Override
            public void Handle(JSONObject obj) {
                try {
                    obj = obj.getJSONObject("data").getJSONObject("post");
                    JSONArray questions = obj.getJSONArray("questions");
                    for (int i = 0; i < questions.length(); i++) {
                        Question q = new Question();
                        JSONObject qObj = questions.getJSONObject(i);
                        q.setText(qObj.getString("title"));
                        if(qObj.getString("type").equals("MCQ")) {
                            q.setQuestionType(QuestionType.MCQ);
                        } else if (qObj.getString("type").equals("TEXT")) {
                            q.setQuestionType(QuestionType.TEXT);
                        }
                        q.setId(qObj.getInt("id"));
                        q.setMulti_choice(qObj.getBoolean("multiChoice"));
                        q.setOpen_answer(qObj.getBoolean("openAnswers"));
                        List<Option> options = new ArrayList<>();
                        JSONArray opts = qObj.getJSONArray("options");
                        for (int j = 0; j < opts.length(); j++) {
                            Option opt = new Option();
                            opt.setId(opts.getJSONObject(j).getInt("id"));
                            opt.setText(opts.getJSONObject(j).getString("name"));
                            options.add(opt);
                        }
                        q.setOptions(options);
                        questionList.add(q);
                    }
                    start();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, postId);
    }

    private void start() {
        if(questionList.get(position).getQuestionType() == QuestionType.TEXT) {
            textView.setVisibility(View.VISIBLE);
            textView.setText(questionList.get(position).getText());
            textAnswer.setVisibility(View.VISIBLE);
        }
        else{
            if(questionList.get(position).isOpen_answer()) {
                addAns.setVisibility(View.VISIBLE);
            }
            textAnswer.setVisibility(View.INVISIBLE);
            displayQuestion(position, questionList.get(position).isMulti_choice());
        }
    }

    private void sendAnswers() {
        GraphApi.sendAnswers(this, new API.RequestHandler() {
            @Override
            public void Handle(JSONObject obj) {

            }
        }, qas, postId);
    }

    public void displayQuestion(int i, boolean isMultible){
        textView.setText(questionList.get(i).getText());
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        if(isMultible) {
            multiChoiceAdapter = new MultiChoiceAdapter(this, questionList.get(i).getOptions());
            recyclerView.setAdapter(multiChoiceAdapter);
        }
        else {
            adapter = new answerAdapter(this, questionList.get(i).getOptions());
            recyclerView.setAdapter(adapter);
        }


    }

}
