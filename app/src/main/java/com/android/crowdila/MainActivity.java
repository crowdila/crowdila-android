package com.android.crowdila;



import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    HomeAdapter adapter;
    RecyclerView recyclerView;
    List<Post> posts;
    FloatingActionButton fButton;
    int lastid=0,cnt=0;
    boolean hasMore=true,isloading=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        recyclerView = findViewById(R.id.HomeRV);
        fButton = findViewById(R.id.fab);

        fButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this,AddPost.class));
            }
        });
        recyclerView = findViewById(R.id.HomeRV);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(layoutManager);
        posts = new ArrayList<>();
        Load();
    }

    protected void Load() {
        GraphApi.fetchPostFeed(this, new API.RequestHandler() {
            @Override
            public void Handle(JSONObject obj) {
                try {
                    obj = obj.getJSONObject("data");
                    JSONArray arr = obj.getJSONArray("feed");
                    for (int i = 0; i < arr.length(); i++) {
                        Post post = new Post();
                        JSONObject postObj = arr.getJSONObject(i);
                        post.setId(postObj.getInt("id"));
                        post.setTitle(postObj.getString("title"));
                        post.setQuestionCount(postObj.getInt("questionCount"));
                        JSONArray tags = postObj.getJSONArray("tags");
                        List<Tag> tagsList = new ArrayList<>();
                        for (int j = 0; j < tags.length(); j++) {
                            Tag tag = new Tag();
                            tag.setName(tags.getJSONObject(j).getString("name"));
                            tagsList.add(tag);
                        }
                        post.setTags(tagsList);
                        posts.add(post);
                    }

                    loadlist();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        },lastid, 20);
        cnt++;
    }

    protected void loadMore()
    {
        int from = 0;
        if(posts.size() > 0) {
            from = posts.get(posts.size() - 1).getId();
        }
        GraphApi.fetchPostFeed(this, new API.RequestHandler() {
            @Override
            public void Handle(JSONObject obj) {
                try {
                    obj = obj.getJSONObject("data");
                    JSONArray arr = obj.getJSONArray("feed");
                    for (int i = 0; i < arr.length(); i++) {
                        Post post = new Post();
                        JSONObject postObj = arr.getJSONObject(i);
                        post.setId(postObj.getInt("id"));
                        post.setTitle(postObj.getString("title"));
                        post.setQuestionCount(postObj.getInt("questionCount"));
                        JSONArray tags = postObj.getJSONArray("tags");
                        List<Tag> tagsList = new ArrayList<>();
                        for (int j = 0; j < tags.length(); j++) {
                            Tag tag = new Tag();
                            tag.setName(tags.getJSONObject(j).getString("name"));
                            tagsList.add(tag);
                        }
                        post.setTags(tagsList);
                        posts.add(post);
                    }

                    loadlist();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, from, 20);

        loadlist();
        if(!hasMore||isloading)
            return;
        isloading=true;
        Load();
    }

    protected void loadlist()
    {
        isloading=false;
        if(adapter == null)
        {
            adapter = new HomeAdapter(this, posts, recyclerView, new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    int Pid = adapter.getData().get(position).getId();
                    Intent i = new Intent(MainActivity.this,answerQuestion.class);
                    i.putExtra("postID",Pid);
                    startActivity(i);
                }
            });
            adapter.setOnLoadMoreListener(new HomeAdapter.OnLoadMoreListener() {
                @Override
                public void onLoadMore() {
                    loadMore();
                }
            });
            recyclerView.setAdapter(adapter);
        } else {
            adapter.setLoaded();
            adapter.notifyDataSetChanged();
        }
        if(cnt*20 < posts.size())
            hasMore=false;
        lastid=posts.get(posts.size()-1).getId();
        invalidateOptionsMenu();
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        final MenuItem menuItem = menu.findItem(R.id.avatr);
        Thread thread = new Thread(new Runnable(){
            @Override
            public void run(){
                URL url = null;
                try {
                    url = new URL("https://s3.amazonaws.com/crowdila-avatars/14.png");
                    final Bitmap image = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                    MainActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            menuItem.setIcon(new BitmapDrawable(getResources(), image));
                        }
                    });
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_activity_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id==R.id.avatr)
        {
            startActivity(new Intent(MainActivity.this,Profile.class));
        }else if(id==R.id.myPosts)
        {
            startActivity(new Intent(MainActivity.this,MyPosts.class));
        }
        return super.onOptionsItemSelected(item);
    }
}





