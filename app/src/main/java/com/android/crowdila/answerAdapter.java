package com.android.crowdila;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Handler;

public class answerAdapter extends RecyclerView.Adapter<answerAdapter.answerViewHolder> {

    Context cntx ;
    int option_id;
    List<Integer> list;
    List<Option> optionList ;
    int pos = -1;

    public answerAdapter(Context cntx, List<Option> optionList) {
        this.cntx = cntx;
        this.optionList = optionList;
    }

    @NonNull
    @Override
    public answerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext()) ;
        View view = inflater.inflate(R.layout.option, null);
        return new answerAdapter.answerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final answerViewHolder holder, final int position) {
        Option option = optionList.get(position);
        holder.radioButton.setText(option.getText());
        holder.radioButton.setChecked(position == pos);
    }

    @Override
    public int getItemCount() {
        return optionList.size();
    }

    public void rest(){
        optionList.clear();
        notifyDataSetChanged();
    }

    class answerViewHolder extends RecyclerView.ViewHolder {
        RadioButton radioButton  ;

        public answerViewHolder(View itemView) {
            super(itemView);
            radioButton = itemView.findViewById(R.id.radioButton);
            radioButton.setOnClickListener(new CompoundButton.OnClickListener() {
                @Override
                public void onClick(View view) {
                        if(pos!=-1)
                        option_id = optionList.get(pos).getId();
                        pos = getAdapterPosition();
                        notifyDataSetChanged();
                }
            });
        }
    }


    public List<Integer> getOptionId(){
        list = new ArrayList<>();
        list.add(option_id);
        return list;
    }



}
