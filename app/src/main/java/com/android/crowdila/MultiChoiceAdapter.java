package com.android.crowdila;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioButton;

import java.util.ArrayList;
import java.util.List;

public class MultiChoiceAdapter extends RecyclerView.Adapter<MultiChoiceAdapter.answerViewHolder>  {

    Context cntx ;
    List<Option> optionList ;
    List<Integer> option_id;
    int pos = -1;

    public MultiChoiceAdapter(Context cntx, List<Option> optionList) {
        this.cntx = cntx;
        this.optionList = optionList;
        option_id = new ArrayList<>();
    }

    @NonNull
    @Override
    public MultiChoiceAdapter.answerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(cntx) ;
        View view = inflater.inflate(R.layout.muli_choice, null);
        return new MultiChoiceAdapter.answerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MultiChoiceAdapter.answerViewHolder holder, final int position) {
        Option option = optionList.get(position);
        holder.checkBox.setText(option.getText());
        holder.checkBox.setChecked(false);
        if(option_id.contains(optionList.get(position).getId())) {
            holder.checkBox.setChecked(true);
        }
    }

    @Override
    public int getItemCount() {
        return optionList.size();
    }

    public void rest(){
        optionList.clear();
        notifyDataSetChanged();
    }


    class answerViewHolder extends RecyclerView.ViewHolder {
        CheckBox checkBox;

        public answerViewHolder(View itemView) {
            super(itemView);
            checkBox = itemView.findViewById(R.id.checkbox);
            checkBox.setOnClickListener(new CompoundButton.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(getAdapterPosition()>=0&&getAdapterPosition()<getItemCount()) {
                        if(!option_id.contains(optionList.get(getAdapterPosition()).getId()))
                            option_id.add(optionList.get(getAdapterPosition()).getId());
                        else
                            option_id.remove(optionList.get(getAdapterPosition()).getId());
                    }
                }
            });
        }
    }

    public List<Integer> getOptionId(){
        return option_id;
    }



}
