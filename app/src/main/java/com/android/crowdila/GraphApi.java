package com.android.crowdila;

import android.app.Activity;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

/**
 * Created by messam on 07/08/18.
 */

public class GraphApi {
    private static final String tagsQuery = "{\"query\": \" { tags{ name } }\"}";
    private static final String postQuery = "{\"query\": \"query fetchPost($id: Int!){ post(id: $id){ title, tags{ name } questions{ id, title, type, multiChoice, openAnswers, options{ id, name } } } }\", \"variables\": {} }";
    private static final String postSearchTitleQuery = "{\"query\": \"query fetchFeed($limit: Int, $from: Int){ feed(limit: $limit, from: $from){ id title user{ username avatar{ url } } questionCount tags { name } } }\", \"variables\": {}}";
    private static final String myPostsQuery = "{\"query\": \"query fetchFeed($limit: Int, $from: Int){ me { posts(limit: $limit, from: $from){ id title user{ username avatar{ url } } questionCount tags { name } } } }\", \"variables\": {}}";
    private static final String loginQuery = "{\"query\": \"query fetchLogin($username: String!, $password: String!){ login(username: $username, password: $password){ username token } }\", \"variables\": {}}";
    private static final String signupQuery = "{\"query\": \"mutation mutateCreateUser($username: String!, $email:String!, $password: String!){ createUser(user: {username:$username, email:$email, password:$password}){ username token } }\", \"variables\": {}}";
    private static final String updateTagsQuery = "{\"query\": \"mutation mutateUpdateTags($tags: [String!]!){ updateTags(tags: $tags){ username } }\", \"variables\": {}}";
    private static final String sendAnswerQuery = "{\"query\": \"mutation mutateCreateAnswer($postId: Int!, $answer: [QuestionAnswer!]!){ createAnswer(postId: $postId, answer: $answer) }\", \"variables\": {}}";
    private static final String createPostQuery = "{\"query\": \"mutation mutateCreatePost($title:String!, $tags:[String!]!, $questions:[QuestionInput!]!){ createPost(post: {title:$title, tags:$tags, questions:$questions}){ id title user{ username avatar{ url } } questionCount } }\", \"variables\": {}}";
    private static final String getAnswerStats = "{\"query\": \"query fetchStats($postId:Int!){ post(id: $postId) { questions { title type options { name votes } } } }\", \"variables\": {}}";

    public static void fetchTags(Activity context, API.RequestHandler handler) {
        API.RequestContext ctx = new API.RequestContext(context, tagsQuery, handler);
        new API.Requester().execute(ctx);
    }

    public static void fetchPost(Activity context, API.RequestHandler handler, int postId) {
        try {
            JSONObject query = new JSONObject(postQuery);
            query.getJSONObject("variables").put("id", postId);
            String queryString = query.toString();
            API.RequestContext ctx = new API.RequestContext(context, queryString, handler);
            new API.Requester().execute(ctx);
        } catch (JSONException e) { e.printStackTrace(); }
    }

    public static void fetchPostFeed(Activity context, API.RequestHandler handler, int from, int limit) {
        try {
            JSONObject query = new JSONObject(postSearchTitleQuery);
            query.getJSONObject("variables").put("from", from);
            query.getJSONObject("variables").put("limit", limit);
            String queryString = query.toString();
            API.RequestContext ctx = new API.RequestContext(context, queryString, handler);
            new API.Requester().execute(ctx);
        } catch (JSONException e) { e.printStackTrace(); }
    }

    public static void fetchPostsByMe(Activity context, API.RequestHandler handler, int from, int limit) {
        try {
            JSONObject query = new JSONObject(myPostsQuery);
            query.getJSONObject("variables").put("from", from);
            query.getJSONObject("variables").put("limit", limit);
            String queryString = query.toString();
            API.RequestContext ctx = new API.RequestContext(context, queryString, handler);
            new API.Requester().execute(ctx);
        } catch (JSONException e) { e.printStackTrace(); }
    }

    public static void fetchLogin(Activity context, API.RequestHandler handler, String username, String password) {
        try {
            JSONObject query = new JSONObject(loginQuery);
            query.getJSONObject("variables").put("username", username);
            query.getJSONObject("variables").put("password", password);
            String queryString = query.toString();
            API.RequestContext ctx = new API.RequestContext(context, queryString, handler);
            new API.Requester().execute(ctx);
        } catch (JSONException e) { e.printStackTrace(); }
    }

    public static void fetchPostStats(Activity context, API.RequestHandler handler, int postId) {
        try {
            JSONObject query = new JSONObject(getAnswerStats);
            query.getJSONObject("variables").put("postId", postId);
            String queryString = query.toString();
            API.RequestContext ctx = new API.RequestContext(context, queryString, handler);
            new API.Requester().execute(ctx);
        } catch (JSONException e) { e.printStackTrace(); }
    }

    public static void signup(Activity context, API.RequestHandler handler, String email, String username, String password) {
        try {
            JSONObject query = new JSONObject(signupQuery);
            query.getJSONObject("variables").put("username", username);
            query.getJSONObject("variables").put("password", password);
            query.getJSONObject("variables").put("email", email);
            String queryString = query.toString();
            API.RequestContext ctx = new API.RequestContext(context, queryString, handler, "POST");
            new API.Requester().execute(ctx);
        } catch (JSONException e) { e.printStackTrace(); }
    }

    public static void updateTags(Activity context, API.RequestHandler handler, List<Tag> tags) {
        try {
            JSONObject query = new JSONObject(updateTagsQuery);
            JSONArray tagsArr = new JSONArray();
            for (int i = 0; i < tags.size(); i++) {
                tagsArr.put(tags.get(i).getName());
            }
            query.getJSONObject("variables").put("tags", tagsArr);
            String queryString = query.toString();
            API.RequestContext ctx = new API.RequestContext(context, queryString, handler, "POST");
            new API.Requester().execute(ctx);
        } catch (JSONException e) { e.printStackTrace(); }
    }

    public static void sendAnswers(Activity context, API.RequestHandler handler, List<answerQuestion.QuestionAnswer> answers, int postId) {
        try {
            JSONObject query = new JSONObject(sendAnswerQuery);
            JSONArray answerArr = new JSONArray();
            for (int i = 0; i < answers.size(); i++) {
                answerArr.put(answers.get(i).toJson());
            }
            query.getJSONObject("variables").put("answer", answerArr);
            query.getJSONObject("variables").put("postId", postId);
            String queryString = query.toString();
            API.RequestContext ctx = new API.RequestContext(context, queryString, handler, "POST");
            new API.Requester().execute(ctx);
        } catch (JSONException e) { e.printStackTrace(); }
    }

    public static void createPost(Activity context, API.RequestHandler handler, String title, List<Question> qs,List<Tag> tags) {
        try {
            JSONObject query = new JSONObject(createPostQuery);
            JSONArray qArr = new JSONArray();
            for (int i = 0; i < qs.size(); i++) {
                qArr.put(qs.get(i).toJson());
            }
            JSONArray TArr = new JSONArray();
            for (int i = 0; i < tags.size(); i++) {
                TArr.put(tags.get(i).getName());
            }

            query.getJSONObject("variables").put("questions", qArr);
            query.getJSONObject("variables").put("title", title);
            query.getJSONObject("variables").put("tags", TArr);
            String queryString = query.toString();
            API.RequestContext ctx = new API.RequestContext(context, queryString, handler, "POST");
            new API.Requester().execute(ctx);
        } catch (JSONException e) { e.printStackTrace(); }
    }
}
