package com.android.crowdila;

public class Imagee {
    int imageId;
    String url ;

    public Imagee(int imageId, String url) {
        this.imageId = imageId;
        this.url = url;
    }

    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
