package com.android.crowdila;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MyPosts extends AppCompatActivity {

    HomeAdapter adapter;
    RecyclerView recyclerView;
    List<Post> posts;
    int lastid=0,cnt=0;
    boolean hasMore=true,isloading=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_posts);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        recyclerView = findViewById(R.id.MyPostsRV);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(layoutManager);
        posts = new ArrayList<>();
        Load();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    protected void Load() {
        GraphApi.fetchPostsByMe(this, new API.RequestHandler() {
            @Override
            public void Handle(JSONObject obj) {
                try {
                    obj = obj.getJSONObject("data");
                    JSONArray arr = obj.getJSONObject("me").getJSONArray("posts");
                    for (int i = 0; i < arr.length(); i++) {
                        Post post = new Post();
                        JSONObject postObj = arr.getJSONObject(i);
                        post.setId(postObj.getInt("id"));
                        post.setTitle(postObj.getString("title"));
                        post.setQuestionCount(postObj.getInt("questionCount"));
                        JSONArray tags = postObj.getJSONArray("tags");
                        List<Tag> tagsList = new ArrayList<>();
                        for (int j = 0; j < tags.length(); j++) {
                            Tag tag = new Tag();
                            tag.setName(tags.getJSONObject(j).getString("name"));
                            tagsList.add(tag);
                        }
                        post.setTags(tagsList);
                        posts.add(post);
                    }

                    loadlist();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        },lastid, 20);
        cnt++;
    }

    protected void loadMore()
    {
        int from = 0;
        if(posts.size() > 0) {
            from = posts.get(posts.size() - 1).getId();
        }
        GraphApi.fetchPostFeed(this, new API.RequestHandler() {
            @Override
            public void Handle(JSONObject obj) {
                try {
                    obj = obj.getJSONObject("data");
                    JSONArray arr = obj.getJSONArray("feed");
                    for (int i = 0; i < arr.length(); i++) {
                        Post post = new Post();
                        JSONObject postObj = arr.getJSONObject(i);
                        post.setId(postObj.getInt("id"));
                        post.setTitle(postObj.getString("title"));
                        post.setQuestionCount(postObj.getInt("questionCount"));
                        JSONArray tags = postObj.getJSONArray("tags");
                        List<Tag> tagsList = new ArrayList<>();
                        for (int j = 0; j < tags.length(); j++) {
                            Tag tag = new Tag();
                            tag.setName(tags.getJSONObject(j).getString("name"));
                            tagsList.add(tag);
                        }
                        post.setTags(tagsList);
                        posts.add(post);
                    }

                    loadlist();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, from, 20);

        loadlist();
        if(!hasMore||isloading)
            return;
        isloading=true;
        Load();
    }

    protected void loadlist()
    {
        isloading=false;
        if(adapter == null)
        {
            adapter = new HomeAdapter(this, posts, recyclerView, new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    int Pid = adapter.getData().get(position).getId();
                    Intent i = new Intent(MyPosts.this,MyPost.class);
                    i.putExtra("postID",Pid);
                    startActivity(i);
                }
            });
            adapter.setOnLoadMoreListener(new HomeAdapter.OnLoadMoreListener() {
                @Override
                public void onLoadMore() {
                    loadMore();
                }
            });
            recyclerView.setAdapter(adapter);
        } else {
            adapter.setLoaded();
            adapter.notifyDataSetChanged();
        }
        if(cnt*20 < posts.size())
            hasMore=false;
        lastid=posts.get(posts.size()-1).getId();
        invalidateOptionsMenu();
    }

}
