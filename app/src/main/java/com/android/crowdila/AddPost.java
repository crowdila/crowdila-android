package com.android.crowdila;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.google.android.flexbox.FlexDirection;
import com.google.android.flexbox.FlexWrap;
import com.google.android.flexbox.FlexboxLayoutManager;
import com.google.android.flexbox.JustifyContent;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class AddPost extends AppCompatActivity {

    RecyclerView recyclerView;
    AddPostAdapter adapter;
    List<Question> data;
    List<Tag> tags,alltags;
    Button addQ;
    Post post;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_post);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        data = new ArrayList<>();
        post = new Post();
        tags = new ArrayList<>();
        alltags = new ArrayList<>();
        recyclerView = findViewById(R.id.addPostRV);
        addQ = findViewById(R.id.addQ);
        addQ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adapter.addItem(new Question());
            }
        });
        loadlist();
        load();
    }

    protected void load()
    {
        GraphApi.fetchTags(this, new API.RequestHandler() {
            @Override
            public void Handle(JSONObject obj) {
                try {
                    obj = obj.getJSONObject("data");
                    JSONArray arr = obj.getJSONArray("tags");
                    for (int i = 0; i < arr.length(); i++) {
                        Tag t = new Tag();
                        t.setName(arr.getJSONObject(i).getString("name"));
                        alltags.add(t);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


    void loadlist()
    {
        adapter = new AddPostAdapter(this,data,recyclerView);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(layoutManager);
        SwipeAndDragHelper swipeAndDragHelper = new SwipeAndDragHelper(adapter);
        ItemTouchHelper touchHelper = new ItemTouchHelper(swipeAndDragHelper);
        adapter.setTouchHelper(touchHelper);
        touchHelper.attachToRecyclerView(recyclerView);
        recyclerView.setAdapter(adapter);
        adapter.addItem(new Question());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.add_post_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.publishPost)
        {
            data = adapter.getData();
            post.setQuestions(data);
            post.setTags(tags);
            if(data.size()==0)
            {
                AlertDialog.Builder ad = new AlertDialog.Builder(AddPost.this);
                ad.setTitle("Error");
                ad.setMessage("There is no data to publish, please add questions");
                ad.setNeutralButton("OK",null);
                ad.show();
            }else if(post.getTags().size()==0) {
                AlertDialog.Builder ad = new AlertDialog.Builder(AddPost.this);
                ad.setTitle("Error");
                ad.setMessage("There is no tags, please add some");
                ad.setNeutralButton("OK",null);
                ad.show();
            }else if(data.size()==1)
            {
                post.setTitle(post.getQuestions().get(0).getText());
                saveData();
            }else
            {
                final AlertDialog.Builder ad = new AlertDialog.Builder(AddPost.this);
                ad.setTitle("Post Title");
                final EditText input = new EditText(AddPost.this);
                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT);
                input.setFilters(new InputFilter[]{new InputFilter.LengthFilter(140)});
                input.setLayoutParams(lp);
                ad.setView(input);
                input.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        if(s.length()>0)
                        {

                        }
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                });
                ad.setPositiveButton("done", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String title = input.getText().toString().trim();
                        post.setTitle(title);
                        saveData();
                    }
                });
                ad.setNegativeButton("cancel",null);
                ad.show();
            }
        }else if(id == R.id.addTags)
        {
            tags.clear();
            RecyclerView rv = new RecyclerView(AddPost.this);
            FlexboxLayoutManager layoutManager = new FlexboxLayoutManager(this);
            layoutManager.setFlexDirection(FlexDirection.ROW);
            layoutManager.setJustifyContent(JustifyContent.SPACE_EVENLY);
            layoutManager.setFlexWrap(FlexWrap.WRAP);
            rv.setLayoutManager(layoutManager);
            final TagAdapter ad = new TagAdapter(this,alltags);
            rv.setAdapter(ad);
            AlertDialog.Builder AD = new AlertDialog.Builder(AddPost.this);
            AD.setTitle("Select Tags");
            AD.setView(rv);
            AD.setPositiveButton("Done", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    List<Tag>tt = ad.getTagsList();
                    for(int i=0;i<tt.size();i++)
                        if(tt.get(i).isSelected())
                            tags.add(tt.get(i));
                    post.setTags(tags);
                }
            });
            AD.setNegativeButton("Cancel",null);
            AD.show();

        }
        return super.onOptionsItemSelected(item);
    }

    void saveData()
    {
        GraphApi.createPost(this, new API.RequestHandler() {
            @Override
            public void Handle(JSONObject obj) {
                if(obj!=null&&obj.has("data"))
                    onBackPressed();
                else
                {
                    AlertDialog.Builder ad = new AlertDialog.Builder(AddPost.this);
                    ad.setTitle("Error");
                    ad.setMessage("Something wrong please try again.");
                    ad.setNeutralButton("OK",null);
                    ad.show();
                }
            }
        }, post.getTitle(), data,post.getTags());
    }
}
