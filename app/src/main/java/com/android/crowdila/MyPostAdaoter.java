package com.android.crowdila;

import android.app.Activity;
import android.app.AlertDialog;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class MyPostAdaoter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    @NonNull

    List<Pair<String,String>> list;
    Activity activity;

    public MyPostAdaoter(List<Pair<String,String>>list,Activity activity)
    {
        this.list=list;
        this.activity=activity;
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater;
        inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.my_post_item,null);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final ViewHolder vholder = (ViewHolder) holder;
        ((ViewHolder) holder).tv.setText(list.get(position).first);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    protected class ViewHolder extends RecyclerView.ViewHolder{
        TextView tv;
        public ViewHolder(View itemView) {
            super(itemView);
            tv=itemView.findViewById(R.id.mypostitem);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final int pos = getAdapterPosition();
                    if(pos>=0&&pos<list.size())
                    {
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                AlertDialog.Builder ad = new AlertDialog.Builder(activity);
                                ad.setTitle(list.get(pos).first);
                                ad.setMessage(list.get(pos).second);
                                ad.setPositiveButton("OK",null);
                                ad.show();
                            }
                        });
                    }
                }
            });
        }
    }
}
