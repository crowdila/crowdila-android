package com.android.crowdila;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.Editable;
import android.text.TextWatcher;
import android.transition.AutoTransition;
import android.transition.TransitionManager;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Switch;

import java.util.List;

public class AddPostAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
        implements SwipeAndDragHelper.ActionCompletionContract {

    @NonNull
    List<Question>data;
    Context context;
    RecyclerView recyclerView;
    int lastPosition = -1 ;
    private ItemTouchHelper touchHelper;

    public AddPostAdapter(Context context,List<Question> items,RecyclerView recyclerView)
    {
        this.context = context;
        this.data = items;
        this.recyclerView = recyclerView;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater;
        inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.question_card_item, parent, false);
        return new ViewHolder(view,new MyCustomEditTextListener());
    }

    private void setAnimation(View viewToAnimate, int position)
    {
        if (position > lastPosition&&lastPosition<data.size()&&!data.get(position).isAnimated())
        {
            data.get(position).setAnimated(true);
            Animation animation = AnimationUtils.loadAnimation(context, R.anim.push_item);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

    public void addItem(Question q)
    {
        data.add(q);
        notifyDataSetChanged();
        recyclerView.smoothScrollToPosition(data.size()-1);
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder vholder, final int position) {
        final ViewHolder holder = (ViewHolder) vholder;
        final boolean isExpanded = data.get(position).isSelected();
        holder.myCustomEditTextListener.updatePosition(position);
        holder.clayout.setVisibility(isExpanded?View.VISIBLE:View.GONE);
        holder.itemView.setActivated(isExpanded);

        if(data.get(position).getQuestionType()==QuestionType.TEXT)
        {
            holder.cbOQ.setVisibility(View.GONE);
            holder.cbMQ.setVisibility(View.GONE);
            holder.rv.setVisibility(View.GONE);
            holder.aSwitch.setChecked(false);
            holder.aSwitch.setText("Text ");
        }else
        {
            holder.cbOQ.setVisibility(View.VISIBLE);
            holder.cbMQ.setVisibility(View.VISIBLE);
            holder.rv.setVisibility(View.VISIBLE);
            holder.aSwitch.setChecked(true);
            holder.aSwitch.setText("MCQ ");
        }
        if(data.get(position).isSelected()&&holder.imb.getRotation()==0F)
        {
            float deg = (holder.imb.getRotation() == 180F) ? 0F : 180F;
            holder.imb.animate().rotation(deg).setInterpolator(new AccelerateDecelerateInterpolator());
        }
        holder.cbMQ.setChecked(data.get(position).isMulti_choice());
        holder.cbOQ.setChecked(data.get(position).isOpen_answer());
        holder.imb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AutoTransition autoTransition = new AutoTransition();
                autoTransition.setDuration(175);
                TransitionManager.beginDelayedTransition(recyclerView,autoTransition);
                float deg = (holder.imb.getRotation() == 180F) ? 0F : 180F;
                holder.imb.animate().rotation(deg).setInterpolator(new AccelerateDecelerateInterpolator());
                if(!data.get(position).isSelected()) {
                    holder.clayout.setVisibility(View.VISIBLE);
                    holder.itemView.setActivated(true);
                    data.get(position).setSelected(true);
                }
                else {
                    holder.clayout.setVisibility(View.GONE);
                    data.get(position).setSelected(false);
                    holder.itemView.setActivated(false);
                }
            }
        });
        holder.iv.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getActionMasked() == MotionEvent.ACTION_DOWN) {
                    touchHelper.startDrag(holder);
                }
                return false;
            }
        });
        setAnimation(holder.itemView, position);
        Question q = data.get(position);
        holder.et.setText(q.getText());

        final OptionAdapter adapter = new OptionAdapter(context,q.getOptions(),holder.rv);
        holder.rv.setLayoutManager(new LinearLayoutManager(context));
        SwipeAndDragHelper swipeAndDragHelper = new SwipeAndDragHelper(adapter);
        ItemTouchHelper touchHelper = new ItemTouchHelper(swipeAndDragHelper);
        adapter.setTouchHelper(touchHelper);
        touchHelper.attachToRecyclerView(holder.rv);
        holder.rv.setAdapter(adapter);
        holder.cbMQ.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                adapter.setRadrio(!isChecked);
                data.get(position).setMulti_choice(isChecked);
            }
        });
        holder.cbOQ.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                data.get(position).setOpen_answer(isChecked);
            }
        });
        holder.add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adapter.addItem(new Option());
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    public List<Question> getData()
    {
        return data;
    }

    @Override
    public void onViewMoved(int oldPosition, int newPosition) {
        Question targetQ = data.get(oldPosition);
        Question q = new Question(targetQ);
        data.remove(oldPosition);
        data.add(newPosition,q);
        notifyItemMoved(oldPosition,newPosition);
    }

    @Override
    public void onViewSwiped(final int position) {
        AlertDialog.Builder ad = new AlertDialog.Builder(context);
        ad.setTitle("This item will removed");
        ad.setPositiveButton("Remove", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                data.remove(position);
                notifyItemRemoved(position);
            }
        });
        ad.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                notifyItemChanged(position);
            }
        });
        ad.show();
    }

    public void setTouchHelper(ItemTouchHelper touchHelper) {

        this.touchHelper = touchHelper;
    }

    protected class ViewHolder extends RecyclerView.ViewHolder{
        protected EditText et;
        protected ImageButton imb;
        protected ImageView iv;
        protected Button add;
        protected CheckBox cbOQ,cbMQ;
        protected RecyclerView rv;
        protected ConstraintLayout clayout;
        protected Switch aSwitch;
        protected MyCustomEditTextListener myCustomEditTextListener;
        public ViewHolder(View itemView , MyCustomEditTextListener myCustomEditTextListener) {
            super(itemView);
            et = itemView.findViewById(R.id.ctv);
            imb = itemView.findViewById(R.id.cibdrobdown);
            clayout = itemView.findViewById(R.id.innerdata);
            cbOQ = itemView.findViewById(R.id.innerCBOQ);
            cbMQ = itemView.findViewById(R.id.innerCBMQ);
            rv = itemView.findViewById(R.id.innerRV);
            add = itemView.findViewById(R.id.innerAdd);
            iv = itemView.findViewById(R.id.civ);
            aSwitch = itemView.findViewById(R.id.Qtyp);
            this.myCustomEditTextListener = myCustomEditTextListener;
            et.addTextChangedListener(myCustomEditTextListener);

            aSwitch.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int pos=getAdapterPosition();
                    if(pos>=0&&pos<data.size())
                    {
                        boolean isChecked = !(data.get(pos).getQuestionType() == QuestionType.MCQ);
                        data.get(pos).setQuestionType(isChecked?QuestionType.MCQ : QuestionType.TEXT);
                        notifyDataSetChanged();
                    }
                }
            });
        }
    }

    private class MyCustomEditTextListener implements TextWatcher {
        private int position;

        public void updatePosition(int position) {
            this.position = position;
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            // no op
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            data.get(position).setText(charSequence.toString());
        }

        @Override
        public void afterTextChanged(Editable editable) {
            // no op
        }
    }
}
