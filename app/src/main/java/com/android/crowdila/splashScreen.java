package com.android.crowdila;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.os.Handler;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class splashScreen extends AppCompatActivity {

    int startingWidth, startingHeight,
            finalWidth, finalHeight;
    float finalXValue, startingXValue;

    private final int SPLASH_DISPLAY_LENGTH = 1000;
    ImageView image;
    TextView txtView, signUp, signUpbotton ;
    Button button ;
    String username, token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        image = (ImageView) findViewById(R.id.splashImage);
        txtView =  (TextView) findViewById(R.id.textView);
        signUp =  (TextView) findViewById(R.id.signUp);
        button = (Button) findViewById(R.id.button);
        signUpbotton = (TextView) findViewById(R.id.signUp);
        SharedPreferences pref = getSharedPreferences("user", MODE_PRIVATE);
        username = pref.getString("username", null) ;
        token = pref.getString("token", null);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), Login.class));
                finish();
            }
        });
        signUpbotton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            startActivity(new Intent(getApplicationContext(), SignUp.class));
            finish();
            }
        });



        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

//                Intent mainIntent = new Intent(splashScreen.this, null);
//                splashScreen.this.startActivity(mainIntent);
                if(username != null && token != null){
                    startActivity(new Intent(getApplicationContext(), MainActivity.class));
                    finish();
                }
                else {
                    initialize();
                    animate();

                    move(image, "translationY", 2000, -400);
                    move(txtView, "translationY", 2000, -400);
                    move(button, "translationY", 2000, -200);
                    move(signUp, "translationY", 2000, -200);

                    fadeAnimation(txtView, 2000);
                    fadeAnimation(button, 2000);
                    fadeAnimation(signUp, 2000);
                }

            }
        }, SPLASH_DISPLAY_LENGTH);


    }


    public void move(View view,String coordinate, int time, float to) {

        ObjectAnimator animation = ObjectAnimator.ofFloat(view, coordinate, to);
        animation.setDuration(time);
        animation.start();
    }

    public void fadeAnimation(View view, int time){
        view.setVisibility(View.VISIBLE);
        AlphaAnimation animation = new AlphaAnimation(0.0f, 1.0f);
        animation.setDuration(time);
        view.startAnimation(animation);

    }
    public void initialize() {
        final Point p = new Point();
        getWindowManager().getDefaultDisplay().getSize(p);

        startingWidth = image.getWidth();
        startingHeight = image.getHeight();
        startingXValue = image.getX();

        finalWidth = p.x / 2;
        finalHeight = p.y - image.getTop() - 300;
        finalXValue = p.x / 2 - finalWidth / 2;
    }


    public void animate() {
        final ValueAnimator vaW = ValueAnimator.ofInt(startingWidth, finalWidth);

        vaW.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                int newWidth = (int) vaW.getAnimatedValue();
                ConstraintLayout.LayoutParams lp = (ConstraintLayout.LayoutParams)
                        image.getLayoutParams();
                lp.width = newWidth;
                image.setLayoutParams(lp);
            }
        });

        final ValueAnimator vaH = ValueAnimator.ofInt(startingHeight, finalHeight);

        vaW.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                int newHeight = (int) vaH.getAnimatedValue();
                ConstraintLayout.LayoutParams lp = (ConstraintLayout.LayoutParams)
                        image.getLayoutParams();
                lp.height = newHeight;
                image.setLayoutParams(lp);
            }
        });

        ObjectAnimator oa = ObjectAnimator.ofFloat(
                image, "X", startingXValue,
                finalXValue);
        AnimatorSet as = new AnimatorSet();
        as.playTogether(vaW, vaH, oa);
        as.setDuration(2000);
        as.start();

    }
}
