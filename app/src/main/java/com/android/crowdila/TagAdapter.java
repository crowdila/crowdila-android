package com.android.crowdila;

import android.content.Context;
import android.graphics.PorterDuff;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.List;
import java.util.ListIterator;

public class TagAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    List<Tag> TagsList;
    Context context;
    public TagAdapter(Context con , List<Tag> TL){
        this.context = con;
        this.TagsList=TL;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater ;
        inflater=LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.tag_item_card,null);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ViewHolder viewHolder = (ViewHolder) holder;
        final Tag t = TagsList.get(position);
        viewHolder.Bt.setText(t.getName());
        if(t.isSelected())
        {
//            viewHolder.Bt.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
            viewHolder.Bt.getBackground().setColorFilter(context.getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_OVER);
            viewHolder.Bt.setTextColor(context.getResources().getColor(R.color.wight));

        }else
        {
//            viewHolder.Bt.setBackgroundColor(context.getResources().getColor(R.color.grey));
            viewHolder.Bt.getBackground().setColorFilter(context.getResources().getColor(R.color.lGrey), PorterDuff.Mode.SRC_OVER);
            viewHolder.Bt.setTextColor(context.getResources().getColor(R.color.wight));
        }
        viewHolder.Bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(t.isSelected())
                    t.setSelected(false);
                else
                    t.setSelected(true);
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return TagsList.size();
    }

    public List<Tag> getTagsList() {
        return TagsList;
    }

    protected class ViewHolder extends RecyclerView.ViewHolder{
        protected Button Bt;

        public ViewHolder(View itemView) {
            super(itemView);
            Bt=itemView.findViewById(R.id.tagButton);
        }
    }
}
