package com.android.crowdila;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Pair;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MyPost extends AppCompatActivity {

    RecyclerView recyclerView;
    int postId;
    List<Pair<String,String>>list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_post);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        postId = getIntent().getIntExtra("postID",0);

        recyclerView = findViewById(R.id.MyPostRV);
        list = new ArrayList<>();
        load();

    }

    void load(){
        GraphApi.fetchPostStats(MyPost.this, new API.RequestHandler() {
            @Override
            public void Handle(JSONObject obj) {
                try {
                    obj = obj.getJSONObject("data").getJSONObject("post");
                    JSONArray ques = obj.getJSONArray("questions");
                    for(int i=0;i<ques.length();i++)
                    {
                        String title = ques.getJSONObject(i).getString("title");
                        String answers="";
                        JSONArray ans = ques.getJSONObject(i).getJSONArray("options");
                        for(int j=0;j<ans.length();j++)
                        {
                            answers+=ans.getJSONObject(j).getString("name");
                            answers+=" : ";
                            answers+=ans.getJSONObject(j).getString("votes");
                            answers+="\n";
                        }
                        if(ques.getJSONObject(i).getString("type").equals("MCQ"))
                            list.add(Pair.create(title,answers));
                    }

                    recyclerView.setLayoutManager(new LinearLayoutManager(MyPost.this));
                    MyPostAdaoter adaoter = new MyPostAdaoter(list,MyPost.this);
                    recyclerView.setAdapter(adaoter);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        },postId);
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
