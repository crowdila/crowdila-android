package com.android.crowdila;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

public class Login extends AppCompatActivity {

    TextView username, password;
    Button login;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        username = findViewById(R.id.username);
        password = findViewById(R.id.password);
        login = findViewById(R.id.login);
        login.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                validate(username.getText().toString(), password.getText().toString()); // TODO: loading
            }
        });
    }

    private void validate(String username, String password) {
        GraphApi.fetchLogin(this, new API.RequestHandler() {
            @Override
            public void Handle(JSONObject obj) {
                try {
                    obj = obj.getJSONObject("data").getJSONObject("login");
                    String username = obj.getString("username");
                    String token = obj.getString("token");
                    SharedPreferences pref = Login.this.getSharedPreferences("user", MODE_PRIVATE);
                    SharedPreferences.Editor editor = pref.edit();
                    editor.putString("username", username);
                    editor.putString("token", token);
                    editor.apply();
                    AuthHandler.loadAuth(Login.this);
                    startActivity(new Intent(getApplicationContext(),MainActivity.class));
                    finish();
                } catch (JSONException e) {
                    e.printStackTrace();
                    // TODO: add wrong username/password
                }
            }
        }, username, password);
    }
}
