package com.android.crowdila;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Question {
    private String text="";
    private int id=0;
    private boolean open_answer=false;
    private boolean multi_choice=false;
    private boolean selected=false;
    private boolean animated=false;
    private Post post;
    private QuestionType questionType=QuestionType.MCQ;
    private List<Option> options=new ArrayList<>();

    public Question(){}

    public Question(Question q)
    {
        text = q.text;
        id = q.id;
        open_answer = q.open_answer;
        multi_choice = q.multi_choice;
        selected = q.selected;
        animated = q.animated;
        post = q.post;
        questionType = q.questionType;
        options = q.options;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public boolean isAnimated() {
        return animated;
    }

    public void setAnimated(boolean animated) {
        this.animated = animated;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isOpen_answer() {
        return open_answer;
    }

    public void setOpen_answer(boolean open_answer) {
        this.open_answer = open_answer;
    }

    public boolean isMulti_choice() {
        return multi_choice;
    }

    public void setMulti_choice(boolean multi_choice) {
        this.multi_choice = multi_choice;
    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    public QuestionType getQuestionType() {
        return questionType;
    }

    public void setQuestionType(QuestionType questionType) {
        this.questionType = questionType;
    }

    public List<Option> getOptions() {
        return options;
    }

    public void setOptions(List<Option> options) {
        this.options = options;
    }

    public void addOption(Option option)
    {
        this.options.add(option);
    }

    public JSONObject toJson() {
        JSONObject ret = new JSONObject();
        try {
            ret.put("title", text);
            if(questionType == QuestionType.MCQ) {
                ret.put("type", "MCQ");
            } else {
                ret.put("type", "TEXT");
            }
            ret.put("multiChoice", multi_choice);
            ret.put("openAnswers", open_answer);
            JSONArray opts = new JSONArray();
            for (int i = 0; i < options.size(); i++) {
                opts.put(options.get(i));
            }
            ret.put("options", opts);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return ret;
    }
}


enum QuestionType{
    MCQ,TEXT
}