package com.android.crowdila;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

public class SignUp extends AppCompatActivity {

    Button signUpButton;
    TextView username, password, email ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        username = (TextView)findViewById(R.id.username);
        email = (TextView)findViewById(R.id.email);
        password = (TextView)findViewById(R.id.password);
        signUpButton = (Button) findViewById(R.id.signUp);

        signUpButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            validate(email.getText().toString(), username.getText().toString(), password.getText().toString());
            // TODO: Add loading
            }
        });
    }
    private void validate(String email, String username, String password) {
        GraphApi.signup(this, new API.RequestHandler() {
            @Override
            public void Handle(JSONObject obj) {
                try {
                    obj = obj.getJSONObject("data").getJSONObject("createUser");
                    String username = obj.getString("username");
                    String token = obj.getString("token");
                    SharedPreferences pref = SignUp.this.getSharedPreferences("user", MODE_PRIVATE);
                    SharedPreferences.Editor editor = pref.edit();
                    editor.putString("username", username);
                    editor.putString("token", token);
                    editor.apply();
                    AuthHandler.loadAuth(SignUp.this);
                    Intent i = new Intent(getApplicationContext(),TagsActivity.class);
                    i.putExtra("FROM", "SignUp");
                    startActivity(i);
                    finish();
                } catch (JSONException e) {
                    e.printStackTrace();
                    // TODO: add error state
                }
            }
        }, email, username, password);
    }
}
