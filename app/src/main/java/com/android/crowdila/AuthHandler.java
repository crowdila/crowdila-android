package com.android.crowdila;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by messam on 07/08/18.
 */

public class AuthHandler {
    private static Auth auth;

    public static class Auth {
        public String username;
        public String token;
        public Boolean authenticated;

        public Auth(String un, String tk) {
            username = un;
            token = tk;
            authenticated = un != null && tk != null;
        }

        public String Header() {
            return "Token token=\"" + token + "\",username=\"" + username + "\"";
        }
    }

    public static Auth loadAuth(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("user", Context.MODE_PRIVATE);
        String username = prefs.getString("username", null);
        String token = prefs.getString("token", null);
        auth = new Auth(username, token);
        return auth;
    }

    public static Auth getAuth() {
        return auth;
    }
}
