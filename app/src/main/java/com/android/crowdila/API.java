package com.android.crowdila;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;

public class API {
    private static final String baseUrl = "http://ec2-18-212-16-122.compute-1.amazonaws.com";

    public static String normalRequest(String method, String body, AuthHandler.Auth auth) {
        String result = "";
        HttpURLConnection connection = null;
        BufferedReader reader = null;

        try {
            URL url = new URL(baseUrl);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod(method);
            connection.setRequestProperty("Connection", "Keep-Alive");
            connection.setRequestProperty("Content-Type", "application/json");
            if (auth.authenticated) {
                connection.setRequestProperty("Authorization", auth.Header());
            }
            connection.setDoOutput(true);
            connection.setChunkedStreamingMode(0);

            connection.connect();

            OutputStream writeStream = connection.getOutputStream();
            writeStream.write(body.getBytes());

            int code = connection.getResponseCode();
            InputStream stream;
            if (code < 200 || code >= 300) {
                stream = connection.getErrorStream();
            } else {
                stream = connection.getInputStream();

            }

            reader = new BufferedReader(new InputStreamReader(stream));

            StringBuffer buffer = new StringBuffer();
            String line;

            while ((line = reader.readLine()) != null) {
                buffer.append(line + "\n");
            }

            result = buffer.toString();
            connection.disconnect();

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    public interface RequestHandler {
        void Handle(JSONObject obj);
    }

    public static class RequestContext {
        public Activity context;
        public String body;
        public RequestHandler handler;
        public String method;

        public RequestContext(Activity ctx, String bd, RequestHandler hnd) {
            context = ctx;
            body = bd;
            handler = hnd;
            method = "GET";
        }

        public RequestContext(Activity ctx, String bd, RequestHandler hnd, String mthd) {
            context = ctx;
            body = bd;
            handler = hnd;
            method = mthd;
        }
    }

    public static class Requester extends AsyncTask<RequestContext, String, String> {
        private RequestContext ctx;
        ProgressDialog pd;

        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(RequestContext ...context) {
            ctx = context[0];
            ctx.context.runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    pd = new ProgressDialog(ctx.context);
                    pd.setIcon(ctx.context.getResources().getDrawable(R.drawable.photo_shutter));
                    pd.setTitle("Loading...");
                    pd.show();
                }
            });
            Log.d("OutgoingData", ctx.body);
            return normalRequest(ctx.method, ctx.body, AuthHandler.loadAuth(ctx.context));
        }


        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Log.d("IncomingData", result);

            if (ctx.context.isDestroyed()) {
                return;
            }
            if(pd!=null&&pd.isShowing())
                pd.cancel();

            try {
                final JSONObject obj = new JSONObject(result);
                ctx.context.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ctx.handler.Handle(obj);
                    }
                });
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
