package com.android.crowdila;

public class Option {
    private int id=0,vots=0;
    private String text;
    private boolean isSelected = false;
    private boolean animated=false;

    public Option(){}

    public Option(Option o)
    {
        this.id=o.id;
        this.vots=o.vots;
        this.text=o.text;
        this.isSelected=o.isSelected;
        this.animated=o.animated;
    }


    public boolean isAnimated() {
        return animated;
    }

    public void setAnimated(boolean animated) {
        this.animated = animated;
    }


    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getVots() {
        return vots;
    }

    public void setVots(int vots) {
        this.vots = vots;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

}
