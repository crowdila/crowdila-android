package com.android.crowdila;

import android.content.Context;
import android.media.Image;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.List;

public class logoAdapter extends RecyclerView.Adapter<logoAdapter.logoViewHolder> {

    private Profile cntx;
    private List<Imagee> imageList;
    private ImageView img ;
    private int imgeid;
    public logoAdapter(Profile cntx, List<Imagee> imageList) {
        this.cntx = cntx;
        this.imageList = imageList;
    }

    @NonNull
    @Override
    public logoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(cntx);
        View view = inflater.inflate(R.layout.logo, null);
        return new logoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull logoViewHolder holder, int position) {
        String url = imageList.get(position).getUrl();
        Picasso.get().load(url).fit().into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return imageList.size();
    }

    class logoViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView ;
        public logoViewHolder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.Img);

            imageView.setOnClickListener(new CompoundButton.OnClickListener() {
                @Override
                public void onClick(View view) {
                    img = cntx.findViewById(R.id.CIV);
                    img.setImageDrawable(imageView.getDrawable());
                    RecyclerView recyclerView =cntx.findViewById(R.id.recyclerView);
                    imgeid = imageList.get(getAdapterPosition()).getImageId();
                    recyclerView.setVisibility(View.INVISIBLE);
                }
            });
        }
    }

    public int getImageId(){
        return imgeid;
    }
}
