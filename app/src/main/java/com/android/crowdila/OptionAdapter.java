package com.android.crowdila;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioButton;

import java.util.List;

public class OptionAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
        implements SwipeAndDragHelper.ActionCompletionContract{

    List<Option> options;
    Context context;
    boolean checked,radio=true;
    RecyclerView recyclerView;
    int lastPosition = -1;
    private ItemTouchHelper touchHelper;

    public OptionAdapter(Context context,List<Option> options,RecyclerView recyclerView)
    {
        this.context=context;
        this.options=options;
        this.recyclerView=recyclerView;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater;
        inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.option_card_item,null);
        return new ViewHolder(view,new MyCustomEditTextListener());
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder vholder, final int position) {
        final ViewHolder holder = (ViewHolder) vholder;
        Option o = options.get(position);
        holder.myCustomEditTextListener.updatePosition(position);
        if(radio)
        {
            holder.RB.setVisibility(View.VISIBLE);
            holder.CB.setVisibility(View.GONE);
        }else
        {
            holder.CB.setVisibility(View.VISIBLE);
            holder.RB.setVisibility(View.GONE);
        }
        ((ViewHolder) vholder).ET.setText(o.getText());
        holder.IV.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getActionMasked() == MotionEvent.ACTION_DOWN) {
                    touchHelper.startDrag(holder);
                }
                return false;
            }
        });
        setAnimation(holder.itemView, position);
        holder.IB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                options.remove(position);
                notifyItemRemoved(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return options.size();
    }

    private void setAnimation(View viewToAnimate, int position)
    {
        if (position > lastPosition&&lastPosition<options.size()&&!options.get(position).isAnimated())
        {
            options.get(position).setAnimated(true);
            Animation animation = AnimationUtils.loadAnimation(context, R.anim.push_item);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

    public void addItem(Option o)
    {
        options.add(o);
        notifyDataSetChanged();
        recyclerView.smoothScrollToPosition(options.size()-1);
    }

    public void setRadrio(boolean radio)
    {
        this.radio=radio;
        notifyDataSetChanged();
    }

    public List<Option> getData()
    {
        return options;
    }

    @Override
    public void onViewMoved(int oldPosition, int newPosition) {
        Option targetQ = options.get(oldPosition);
        Option q = new Option(targetQ);
        options.remove(oldPosition);
        options.add(newPosition,q);
        notifyItemMoved(oldPosition,newPosition);
    }

    @Override
    public void onViewSwiped(final int position) {
        AlertDialog.Builder ad = new AlertDialog.Builder(context);
        ad.setTitle("This item will removed");
        ad.setPositiveButton("Remove", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                options.remove(position);
                notifyItemRemoved(position);
            }
        });
        ad.setNegativeButton("Cancel",null);
        ad.show();
    }

    public void setTouchHelper(ItemTouchHelper touchHelper) {

        this.touchHelper = touchHelper;
    }

    protected class ViewHolder extends RecyclerView.ViewHolder{
        protected CheckBox CB;
        protected RadioButton RB;
        protected EditText ET;
        protected ImageView IV;
        protected ImageButton IB;
        protected MyCustomEditTextListener myCustomEditTextListener;
        public ViewHolder(View itemView,MyCustomEditTextListener myCustomEditTextListener) {
            super(itemView);
            CB = itemView.findViewById(R.id.optionItemCB);
            RB = itemView.findViewById(R.id.optionItemRB);
            ET = itemView.findViewById(R.id.optionItemET);
            IV = itemView.findViewById(R.id.optionItemIV);
            this.myCustomEditTextListener=myCustomEditTextListener;
            ET.addTextChangedListener(myCustomEditTextListener);
            IB = itemView.findViewById(R.id.optionItemIB);
        }
    }

    private class MyCustomEditTextListener implements TextWatcher {
        private int position;

        public void updatePosition(int position) {
            this.position = position;
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            // no op
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            options.get(position).setText(charSequence.toString());
        }

        @Override
        public void afterTextChanged(Editable editable) {
            // no op
        }
    }
}
